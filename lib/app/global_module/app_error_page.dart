import 'package:flutter/material.dart';
class AppErrorPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Error',
      home: _buildAppErrorPage(),
    );
  }

  _buildAppErrorPage(){
    return Scaffold(
      body: Center(
        child: Text("Application Error. Please Reopen App"),
      ),
    );
  }
}