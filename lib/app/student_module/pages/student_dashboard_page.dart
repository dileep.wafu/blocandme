import 'package:change_not_pro_ive/app/auth_module/blocs/auth_bloc.dart';
import 'package:change_not_pro_ive/app/router_module/theme_router/app_theme.dart';
import 'package:change_not_pro_ive/app/router_module/theme_router/bloc/theme_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StudentDashboard extends StatefulWidget {
  @override
  _StudentDashboardState createState() => _StudentDashboardState();
}

class _StudentDashboardState extends State<StudentDashboard> {
  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);
    final themeBloc = Provider.of<ThemeBloc>(context);

    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Text("Student Logged in"),
              RaisedButton(
                onPressed: () {
                  authBloc.logout();
                },
                child: Text('logout'),
              ),
              SizedBox(
                height: 30,
              ),
              RaisedButton(
                onPressed: () {
                  print(
                      "@HOME PAGE CURRENT THEME >>> ${themeBloc.currentTheme} ");
                  if (themeBloc.currentTheme == AppTheme.light) {
                    print("@HOME CURRENT APP THEME is LIGHT");
                    themeBloc.setNewTheme(AppTheme.dark);
                  } else {
                    print("@HOME CURRENT APP THEME is DARK");

                    themeBloc.setNewTheme(AppTheme.light);
                  }
                  
                },
                child: Text("Toggle Theme"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
