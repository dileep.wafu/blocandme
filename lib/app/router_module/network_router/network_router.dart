import 'package:change_not_pro_ive/app/global_module/app_error_page.dart';
import 'package:change_not_pro_ive/app/global_module/loading_page.dart';
import 'package:change_not_pro_ive/app/global_module/network_error_page.dart';
import 'package:change_not_pro_ive/app/router_module/onboarding_router/router/onboard_router.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter/material.dart';

//Root Page check for network connectivity
class NetworkRouter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("ON NETWORK ROUTER ::");
    return StreamBuilder<DataConnectionStatus>(
      stream: DataConnectionChecker().onStatusChange,
      builder: (context, snap) {
        if (snap.hasData) {

          if (snap.data == DataConnectionStatus.connected){
            print("Network Connected");
            return OnboardingRouter.create(context); 
            }
          else
            return NetworkErrorPage();
        } else if (snap.hasError)
          return AppErrorPage();
        else
          return LoadingPage();
      },
    );
  }
}
