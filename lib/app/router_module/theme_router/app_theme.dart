import 'package:flutter/material.dart';

enum AppTheme{
  light,
  dark
}


final appThemesData = {
  AppTheme.light: ThemeData(primaryColor: Colors.yellow),
  AppTheme.dark: ThemeData.dark(),
};