
import 'package:change_not_pro_ive/app/auth_module/router/auth_router.dart';
import 'package:change_not_pro_ive/app/global_module/app_error_page.dart';
import 'package:change_not_pro_ive/app/global_module/loading_page.dart';
import 'package:change_not_pro_ive/app/router_module/theme_router/bloc/theme_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//here we will write provider that will provide to enitre tree.
class ThemeRouter extends StatefulWidget {

  //for code maintainability
  static Widget create(BuildContext context) {
    return Provider<ThemeBloc>(
      create: (_) => ThemeBloc(),
      child: ThemeRouter(),
      dispose: (context,bloc)=>bloc.dispose(),
    );
  }

  @override
  _ThemeRouterState createState() => _ThemeRouterState();
}

class _ThemeRouterState extends State<ThemeRouter> {
  @override
  Widget build(BuildContext context) {
    final themeBloc = Provider.of<ThemeBloc>(context);
    themeBloc.initTheme();
    print("ON Theme Router");

    return StreamBuilder<ThemeData>(
      stream: themeBloc.appThemeStream,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          print("Going to Auth Router");
          return MaterialApp(
            title: 'Bloc ThemeData',
            theme: snapshot.data,
            home: AuthRouter.create(context),
          );
          
        } else if (snapshot.hasError)
          return AppErrorPage();
        else
          return LoadingPage();
      },
    );
  }
}
