
import 'package:change_not_pro_ive/app/router_module/theme_router/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:rxdart/rxdart.dart';

class ThemeBloc {
  //create streamController
  BehaviorSubject<ThemeData> _themeDataStreamController = BehaviorSubject<ThemeData>();
  AppTheme _currentTheme;
  //expose Stream
  Stream<ThemeData> get appThemeStream => _themeDataStreamController.stream;
  AppTheme get currentTheme => _currentTheme;
  Box box;

  //dispose
  void dispose(){
    _themeDataStreamController.close();
  }

  //initialize Stream
  void initTheme(){
    box = Hive.box('appDB');
    int themeIndex = box.get('themeIndex');

    if(themeIndex == null) {
      print("Putting 0 to themeIndex in DB");
      themeIndex = 0;
      box.put('themeIndex', 0);
    }

    print("THEMEINDEX GOT from DB:::: $themeIndex");
    //default light theme setting.
    _themeDataStreamController.add(appThemesData[AppTheme.values[themeIndex]]);
    // print("Initializing the themedata Stream");
    //later get the theme data index from db and set to stream.
    _currentTheme = AppTheme.values[themeIndex];
  }

  //set new theme. add to stream
  void setNewTheme(AppTheme theme) async{
    print("REQUEST THEME ... ");
    //add new theme to stream
    _themeDataStreamController.add(appThemesData[theme]);
    print("SET NEW THEME TO DB and current theme::: $theme");
    _currentTheme = theme;
    //later set the Db theme index\
    int newThemeIndex = theme.index;
    print("SETTING THEMEINDEX to db :: $newThemeIndex ");
    //put await because for consistency.
    await box.put('themeIndex', newThemeIndex);

  }

    
}