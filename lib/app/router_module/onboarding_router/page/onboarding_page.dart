import 'package:change_not_pro_ive/app/router_module/onboarding_router/bloc/onboarding_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OnboardingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'App Onboarding',
      home: _buildOnboardingPage(context),
    );
  }

  _buildOnboardingPage(context){
    OnboardingBloc onboardingBloc = Provider.of<OnboardingBloc>(context);
    return Scaffold(
      body: Container(
        child: Center(
          child: Column(
            children: <Widget>[
              Text("Onboarding page for app"),
              RaisedButton(onPressed: (){ 
                onboardingBloc.doneWithOnboard();
              },
              child: Text("Skip this page"),
              )
            ],
          ),
        ),
      ),
    );
  }
}