import 'package:hive/hive.dart';
import 'package:rxdart/rxdart.dart';

class OnboardingBloc {
  BehaviorSubject<bool> _onboardingStreamController = BehaviorSubject<bool>();
  Stream<bool> get onboardingStream => _onboardingStreamController.stream;

  Box box;

  //dispose
  void dispose() {
    _onboardingStreamController.close();
  }

  //initialize Stream
  void initOnboard() {
    box = Hive.box('appDB');
    bool doesOnboarded = box.get('doesOnboarded');
    
    if (doesOnboarded == null) {
      print("Putting false to onBoardValue in DB");
      doesOnboarded = false;
      box.put('doesOnboarded', false);
    }

    _onboardingStreamController.add(doesOnboarded);
  }

  void doneWithOnboard() async {
    await box.put('doesOnboarded', true);
    _onboardingStreamController.add(true);
  }
}
