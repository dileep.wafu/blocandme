import 'package:change_not_pro_ive/app/global_module/app_error_page.dart';
import 'package:change_not_pro_ive/app/global_module/loading_page.dart';
import 'package:change_not_pro_ive/app/router_module/onboarding_router/bloc/onboarding_bloc.dart';
import 'package:change_not_pro_ive/app/router_module/onboarding_router/page/onboarding_page.dart';
import 'package:change_not_pro_ive/app/router_module/theme_router/router/theme_router.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OnboardingRouter extends StatefulWidget {

  //for code maintainability
  //It will create provider that will provided to entire tree .
  static Widget create(BuildContext context) {
    return Provider<OnboardingBloc>(
      create: (_) => OnboardingBloc(),
      child: OnboardingRouter(),
      dispose: (context,bloc)=>bloc.dispose(),
    );
  }

  @override
  _OnboardingRouterState createState() => _OnboardingRouterState();
}


/*
  This OnBoardRouter will do ::
    If user has already onboarded >>> Take him to "ThemeRouter"
    If user no onboarded >>>> Take him to "onboardPage"
    Else >> AppError or Loading... 
    
*/


class _OnboardingRouterState extends State<OnboardingRouter> {

  @override
  Widget build(BuildContext context) {
    OnboardingBloc onboardingBloc = Provider.of<OnboardingBloc>(context);
    onboardingBloc.initOnboard();
    return StreamBuilder(
      stream: onboardingBloc.onboardingStream,
      builder: (context, snap){
        if (snap.hasData) {
          //means the preson is not yet onboarded.
          if (snap.data == false)
            return OnboardingPage();
          else
            return ThemeRouter.create(context);
        } else if (snap.hasError)
          return AppErrorPage();
        else
          return LoadingPage();
      },
    );
  }
}