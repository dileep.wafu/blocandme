import 'package:change_not_pro_ive/app/router_module/network_router/network_router.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return NetworkRouter();
  }
}