import 'package:change_not_pro_ive/app/auth_module/blocs/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);
    
    return Scaffold(
      body: Column(
        children: <Widget>[
          TextFormField(
      
          ),
          TextFormField(),
          RaisedButton(onPressed: (){
            print("Pressed login.");
            authBloc.loginWithUsernamePassword('dileep','test');
          },child: Text("Login"),
          
          )
        ],
      ),
    );
  }
}