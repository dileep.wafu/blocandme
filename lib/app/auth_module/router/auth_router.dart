import 'package:change_not_pro_ive/app/auth_module/blocs/auth_bloc.dart';
import 'package:change_not_pro_ive/app/auth_module/pages/login_page.dart';
import 'package:change_not_pro_ive/app/global_module/app_error_page.dart';
import 'package:change_not_pro_ive/app/global_module/loading_page.dart';
import 'package:change_not_pro_ive/app/student_module/pages/student_dashboard_page.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AuthRouter extends StatefulWidget {
  static Widget create(BuildContext context) {
    return Provider<AuthBloc>(
      create: (_) => AuthBloc(),
      child: AuthRouter(),
      dispose: (context, bloc) => bloc.dispose(),
    );
  }

  @override
  _AuthRouterState createState() => _AuthRouterState();
}

class _AuthRouterState extends State<AuthRouter> {
  @override
  Widget build(BuildContext context) {
    final authBloc = Provider.of<AuthBloc>(context);
    authBloc.initAuthToken();
    return StreamBuilder(
      stream: authBloc.authTokenStream,
      builder: (context, snap) {
        if (snap.hasData) {
          print("Auth token has data");
          if (snap.data == ''){
            print("AUTH TOKEN Nothing,, SO GNG TO LOGIN PAGE");
            return LoginPage(); 
            
            }
          else
            return StudentDashboard();
        } else if (snap.hasError)
          return AppErrorPage();
        else
          return LoadingPage();
      },
    );
  }
}
