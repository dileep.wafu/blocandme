import 'dart:convert';
import 'dart:io';

import 'package:hive/hive.dart';
import 'package:rxdart/rxdart.dart';
import 'package:http/http.dart';

class AuthBloc {
  BehaviorSubject<String> _authTokenStreamController =
      BehaviorSubject<String>();
  Stream<String> get authTokenStream => _authTokenStreamController.stream;

  Box box;

  //dispose
  void dispose() {
    _authTokenStreamController.close();
  }

  //initialize Stream
  void initAuthToken() {
    //open DB
    box = Hive.box('appDB');

    print("AUTH BLOC ::OPENED DB>>>> ");
    //get the authtoken stored in Db earlier
    String authToken = box.get('authToken');
    // print("AUTHTOKEN :: ${authToken}");
    //if we get the auth token, it will be null or token, that will be  added to stream.

    //refacator this code, write only once. no checking.
    if(authToken == null){
      _authTokenStreamController.add('');
    }else{
      _authTokenStreamController.add(authToken);
    }
  }

  // //add auth token to stream and db.
  // void addToken(String token) async{
  //   await box.put('authToken', token);
  //   _authTokenStreamController.add(token);
  // }

  void removeToken() async {
    await box.delete('authToken');
    _authTokenStreamController.add('');
  }

  loginWithUsernamePassword(String username, String password) async {
    //Algo:::
    // make network request on try catch
    //if status is true :: get the token and store on db and add that token to db.
    // if you want to decode token and fetch user roles and profile, fetch it and add it to db. also addToken to Stream also.
    //finally add token to stream.
    //else status is false:: rethrow the message back to called function.

    //testing area below...
    try {
      String url = 'http://103.127.157.59:8626/shikshalayas/login';
  final body = jsonEncode({
        "userId": "IIIT313326",
        "password": "16220242"
      });

      final headers = {'Content-Type':'application/json'};

      Response response = await post(url, headers: headers, body: body);

      print("RESPONSE:::: ${response.body}");

      var decodedBody = jsonDecode(response.body);
      print("DECODED BODY :: $decodedBody");
      //set user details by decoding the token into db and add to a model in this bloc if time.
      print("GOT TOKEN :: ${decodedBody['data']}");
      _authTokenStreamController.add(decodedBody['data']);
      await box.put('authToken', decodedBody['data']);
      print("PUT TOKEN ON DB");
    } catch (e) {
      print("login error :: $e");
    }
  }

  logout() {
    //Algo ::
    //call api to make expire the token and
    //removeToken() >> removes the token from DB.
    //remove user profile also.
    //remove themeIndex too.

    //testing area below ....

    removeToken();

  }
}
