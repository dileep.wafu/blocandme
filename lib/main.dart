
import 'package:change_not_pro_ive/app/app.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:hive/hive.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  print("Initializing HIVE AND OPENING 'appDB'");
  final appDocumentDir = await getApplicationDocumentsDirectory();
  Hive.init(appDocumentDir.path);
  await Hive.openBox('appDB');

  runApp(App());
}

// don't Forgot to close Hive on app dispose. It's a good pattern to follow not compulsory.


/*
App DB::
name of db :: 'appDB'
DATA ::
1. themeIndex
2. onBoardValue
3. userToken
4. userMap : 
  {
    name:
    id: 
    role:
  }



*/
